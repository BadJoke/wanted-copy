﻿using UnityEngine;

public class CarMovement : MonoBehaviour
{
    private float _speed;

    private void Start()
    {
        _speed = Random.Range(1.5f, 2f);
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);
    }
}