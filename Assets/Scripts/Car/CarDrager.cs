﻿using UnityEngine;
using Assets.Scripts;

public class CarDrager : MonoBehaviour
{
    [SerializeField] private CarPositionChecker _positionChecker;

    private Camera _camera;
    private Rigidbody _rigidbody;
    private BoxCollider _collider;
    private Vector3 _startPosition;
    private Vector3 _targetPosition;
    private float _liftSpeed = 3.5f;
    private float _moveSpeed;

    private void Awake()
    {
        _camera = Camera.main;
        _moveSpeed = Options.Sensitivity;
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<BoxCollider>();
    }

    private void OnMouseDown()
    {
        if (Options.IsPlaying)
        {
            _rigidbody.useGravity = false;
            _rigidbody.freezeRotation = true;
            _collider.enabled = false;
            _startPosition = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
            _positionChecker.enabled = true;
        }
    }

    private void OnMouseDrag()
    {
        if (Options.IsPlaying)
        {
            Vector3 liftPosition = new Vector3(transform.position.x, 1.2f, transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, liftPosition, _liftSpeed * Time.deltaTime);

            _targetPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f);
            _targetPosition = _camera.ScreenToWorldPoint(_targetPosition);

            Vector2 offset = new Vector2(_startPosition.x - _targetPosition.x, _startPosition.y - _targetPosition.y);
            Vector3 newPosition = new Vector3(
                transform.position.x - offset.x * _moveSpeed * Time.deltaTime,
                transform.position.y,
                transform.position.z - offset.y * _moveSpeed * Time.deltaTime);

            transform.position = Vector3.MoveTowards(
                transform.position,
                newPosition,
                1f);

            _startPosition = new Vector3(_targetPosition.x, _targetPosition.y, 10f);
        }
    }

    private void OnMouseUp()
    {
        _rigidbody.useGravity = true;
        _rigidbody.freezeRotation = false;
        _collider.enabled = true;
        _positionChecker.enabled = false;
    }
}
