﻿using System;
using UnityEngine;

public class CarPositionChecker : MonoBehaviour
{
    private float _leftBorder = -0.5f;
    private float _rightBorder = 3.5f;
    private float _forwardBorder = -3.5f;
    private float _backwardBorder = -10.5f;
    private float _offsetRatio;

    private void Start()
    {
        _offsetRatio = 1f / (float)Math.Tan(94.626 * Math.PI / 180);
    }

    private void Update()
    {
        float leftOffset = _offsetRatio * (-7 - transform.position.z);
        float rightOffset = leftOffset * 6;

        if (transform.position.x < _leftBorder + leftOffset)
        {
            transform.position = new Vector3(_leftBorder + leftOffset, transform.position.y, transform.position.z);
        }
        if (transform.position.x > _rightBorder + rightOffset)
        {
            transform.position = new Vector3(_rightBorder + rightOffset, transform.position.y, transform.position.z);
        }
        if (transform.position.z > _forwardBorder)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, _forwardBorder);
        }
        if (transform.position.z < _backwardBorder)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, _backwardBorder);
        }
    }
}
