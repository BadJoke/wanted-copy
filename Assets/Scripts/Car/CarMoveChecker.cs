﻿using UnityEngine;

public class CarMoveChecker : MonoBehaviour
{
    [SerializeField] private CarMovement _movement;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Ground") || other.CompareTag("Road") || other.CompareTag("Car"))
        {
            _movement.enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _movement.enabled = false;
    }
}
