﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class CarCatcher : MonoBehaviour
{
    [SerializeField]
    private ScoreBar _scoreBar;
    [SerializeField]
    private Transform _wanted;
    [SerializeField]
    private Cars _cars;
    [SerializeField]
    private CarPooler _pooler;

    [SerializeField]
    private Text _message;
    [SerializeField]
    private GameObject _arrow;


    private void Start()
    {
        ChangeWantedCar();
    }

    public void ChangeWantedCar()
    {
        if (_wanted.childCount > 0)
        {
            GameObject wanted = _wanted.GetChild(0).gameObject;
            Destroy(wanted);
        }

        var car = Instantiate(_pooler.TakeNewWanted(), _wanted).transform;
        car.localPosition = Vector3.zero;
        car.localRotation = Quaternion.Euler(0, 0, 0);
        car.localScale = new Vector3(25, 25, 25);
        car.GetComponent<Rigidbody>().useGravity = false;
        car.GetComponent<BoxCollider>().enabled = false;
        car.GetComponent<CarMovement>().enabled = false;
        car.gameObject.SetActive(true);

        _pooler.SetWanted(car.GetComponent<CarData>());
    }

    private void OnTriggerEnter(Collider other)
    {
        CarData wanted = _wanted.GetChild(0).GetComponent<CarData>();
        CarData car = other.GetComponent<CarData>();

        if (car.Equals(wanted))
        {
            _scoreBar.IncreaseScore();
        }
        else
        {
            if (Options.IsLevelStart)
            {
                _scoreBar.DecreaseScore();
            }
        }

        if (Options.IsLevelStart == false)
        {
            Options.IsLevelStart = true;
            _message.enabled = false;
            _arrow.SetActive(false);
        }

        ChangeWantedCar();

        other.transform.parent = _pooler.transform;
        _pooler.Push(other.gameObject);
    }
}