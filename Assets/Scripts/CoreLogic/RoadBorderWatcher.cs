﻿using Assets.Scripts;
using UnityEngine;

public class RoadBorderWatcher : MonoBehaviour
{
    [SerializeField]
    private ScoreBar _scoreBar;
    [SerializeField]
    private GameObject _wantedCar;
    [SerializeField]
    private Transform _wanted;
    [SerializeField]
    private CarPooler _pooler;
    [SerializeField]
    private CarCatcher _catcher;

    private void OnTriggerEnter(Collider other)
    {
        CarData wanted = _wanted.GetChild(0).GetComponent<CarData>();
        CarData car = other.GetComponent<CarData>();

        if (wanted.Equals(car))
        {
            if (Options.IsLevelStart)
            {
                _scoreBar.DecreaseScore();
            }
            _catcher.ChangeWantedCar();
        }

        other.transform.parent = _pooler.transform;
        _pooler.Push(other.gameObject);
    }
}
