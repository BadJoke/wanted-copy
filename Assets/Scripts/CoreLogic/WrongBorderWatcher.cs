﻿using Assets.Scripts;
using UnityEngine;

public class WrongBorderWatcher : MonoBehaviour
{
    [SerializeField] private ScoreBar _scoreBar;
    [SerializeField] private CarPooler _pooler;
    [SerializeField] private CarCatcher _catcher;

    private void OnTriggerEnter(Collider other)
    {
        other.transform.parent = _pooler.transform;
        _pooler.Push(other.gameObject);

        if (Options.IsLevelStart)
        {
            _scoreBar.DecreaseScore();
        }
        
        _catcher.ChangeWantedCar();
    }
}
