﻿using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;

public class CarPooler : MonoBehaviour
{
    [SerializeField]
    private Cars _cars;

    private int _wantedNotSpawnedCount = 0;
    private int _wantedNotSpawnedMax = 10;

    private CarData _wanted;
    private List<GameObject> _pool = new List<GameObject>();
    private Queue<GameObject> _poolQueue = new Queue<GameObject>();

    private void Start()
    {
        List<GameObject> cars;

        if (Options.ShowingLevel < 3)
        {
            cars = Shuffle(_cars.Prefabs, 28);
        }
        else if (Options.ShowingLevel < 7)
        {
            cars = Shuffle(_cars.Prefabs, 35);
        }
        else if (Options.ShowingLevel < 12)
        {
            cars = Shuffle(_cars.Prefabs, 42);
        }
        else if (Options.ShowingLevel < 17)
        {
            cars = Shuffle(_cars.Prefabs, 49);
        }
        else
        {
            cars = Shuffle(_cars.Prefabs, 56);
        }

        foreach (var car in cars)
        {
            var nonActiveCar = Instantiate(car);
            nonActiveCar.transform.parent = transform;
            nonActiveCar.SetActive(false);
            _pool.Add(nonActiveCar);
            _poolQueue.Enqueue(nonActiveCar);
        }
    }

    private List<GameObject> Shuffle(List<GameObject> list, int amount)
    {
        List<GameObject> oldList = new List<GameObject>();
        List<GameObject> newList = new List<GameObject>();

        for (int i = 0; i < amount; i++)
        {
            oldList.Add(list[i]);
        }

        while (oldList.Count > 0)
        {
            int index = Random.Range(0, oldList.Count);
            newList.Add(oldList[index]);
            oldList.RemoveAt(index);
        }

        return newList;
    }

    public GameObject Take()
    {
        if (_pool[0].GetComponent<CarData>().Equals(_wanted) == false)
        {
            _wantedNotSpawnedCount++;
        }

        if (_wantedNotSpawnedCount < _wantedNotSpawnedMax)
        {
            return TakeFromRange(0,0);
        }
        else
        {
            _wantedNotSpawnedCount = 0;
            int i = _pool.FindIndex(x => x.GetComponent<CarData>().Equals(_wanted));
            i = i > 0 ? i : 0;
            var car = _pool[i];
            _pool.RemoveAt(i);
            return car;
        }
    }

    public GameObject TakeNewWanted()
    {
        return _pool[Random.Range(0, _pool.Count)];
    }

    public GameObject TakeFromRange(int star, int end)
    {
        int i = Random.Range(star, end);
        var car = _pool[i];
        _pool.RemoveAt(i);
        return car;
    }

    public void Push(GameObject car)
    {
        car.SetActive(false);
        _pool.Add(car);
    }

    public void SetWanted(CarData car)
    {
        _wanted = car;
    }
}
