﻿namespace Assets.Scripts
{
    public class Score
    {
        private static Score _instance;
        public int Value { get; private set; }
        public int MaxValue { get; private set; }
        public float BestProgress { get; private set; } 

        private Score(int max)
        {
            Value = 0;
            MaxValue = max;
        }

        public static Score GetInstance()
        {
            if (_instance == null)
            {
                NewInstance(5);
            }

            return _instance;
        }

        public static void NewInstance(int max)
        {
            _instance = new Score(max);
        }

        public void Increase()
        {
            ++Value;
        }

        public void Decrease()
        {
            --Value;
        }

        public float GetProgress()
        {
            float currentProgress = (float)Value / MaxValue;

            if (currentProgress > BestProgress)
            {
                BestProgress = currentProgress;
            }

            return currentProgress;
        }
    }
}
