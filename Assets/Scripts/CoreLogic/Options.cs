﻿namespace Assets.Scripts
{
    public static class Options
    {
        public static float Sensitivity = 35f;
        public static int Level = 0;
        public static int ShowingLevel = 1;
        public static bool IsPlaying = true;
        public static bool IsLevelStart = false;
    }
}
