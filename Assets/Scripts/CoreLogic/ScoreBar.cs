﻿using Assets.Scripts;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBar : MonoBehaviour
{
    [SerializeField] private GameObject _losePanel;
    [SerializeField] private GameObject _winPanel;
    [SerializeField] private Image _progress;

    private Score _score;

    private void Start()
    {
        _score = Score.GetInstance();
    }

    public void IncreaseScore()
    {
        _score.Increase();

        StopCoroutine("FillScoreBar");
        StartCoroutine(FillScoreBar(_progress.fillAmount, _score.GetProgress()));
    }

    public void DecreaseScore()
    {
        if (_score.Value > 0)
        {
            _score.Decrease();

            StopCoroutine("FillScoreBar");
            StartCoroutine(FillScoreBar(_progress.fillAmount, _score.GetProgress()));
        }
        else
        {
            Options.IsPlaying = false;
            _losePanel.SetActive(true);
        }
    }

    private IEnumerator FillScoreBar(float startValue, float finishValue)
    {
        while (Math.Abs(startValue - finishValue) >= 0.01f)
        {
            startValue = Mathf.MoveTowards(startValue, finishValue, 0.01f);
            _progress.fillAmount = startValue;
            yield return null;
        }

        if (_score.GetProgress() == 1)
        {
            Options.IsPlaying = false;
            _winPanel.SetActive(true);
        }
    }
}
