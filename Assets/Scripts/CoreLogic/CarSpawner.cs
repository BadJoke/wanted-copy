﻿using Assets.Scripts;
using System.Collections;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    [SerializeField] private CarPooler _pooler;

    private void Start()
    {
        StartCarSpawn(new Vector3(0, 0, Random.Range(2.5f, 3.1f)));
        StartCarSpawn(new Vector3(0, 0, Random.Range(4.4f, 5.0f)));
        StartCoroutine(SpawnCar());
    }

    private void StartCarSpawn(Vector3 position)
    {
        GameObject car = _pooler.TakeFromRange(0, 0);
        car.transform.position = transform.position;
        car.transform.rotation = transform.rotation;
        car.transform.parent = transform;
        car.transform.Translate(position);
        car.SetActive(true);
    }

    private IEnumerator SpawnCar()
    {
        while (Options.IsPlaying)
        {
            GameObject car = _pooler.Take();
            car.transform.position = transform.position;
            car.transform.rotation = transform.rotation;
            car.transform.parent = transform;
            car.SetActive(true);

            yield return new WaitForSeconds(Random.Range(1f, 1.5f));
        }
    }
}
