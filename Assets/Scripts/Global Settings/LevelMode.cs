﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level Mode", menuName = "Level Mode")]
public class LevelMode : ScriptableObject
{
    [Range(5, 15)]
    public int CarsToWin = 5;
    public List<bool> Roads;
}
