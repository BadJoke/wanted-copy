﻿using Assets.Scripts;
using UnityEngine;

public class CarData : MonoBehaviour
{
    public CarType Type;
    public CarColor Color;

    public bool Equals(CarData car)
    {
        return Type == car.Type && Color == car.Color;
    }
}