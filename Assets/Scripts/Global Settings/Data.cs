﻿namespace Assets.Scripts
{
    public enum CarType { Retro, PickUp, Track, HardTop, Japan, Gelendvagen, HatchBack, SportCar, LoadedTruck }
    public enum CarColor { Gray, Blue, Green, Purple, Red, White, Yellow }
}
