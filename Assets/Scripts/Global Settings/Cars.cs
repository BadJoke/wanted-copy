﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Car List", menuName = "Car List")]
public class Cars : ScriptableObject
{
    public List<GameObject> Prefabs;
}
