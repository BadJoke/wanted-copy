﻿using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCustomizer : MonoBehaviour
{
    [SerializeField] private Text _levelNumber;
    [SerializeField] private List<GameObject> _roads;
    [SerializeField] private List<GameObject> _spawners;
    [SerializeField] private List<GameObject> _signs;
    [SerializeField] private List<LevelMode> _levels;

    public int Level;

    private void Awake()
    {
        LoadPrefs();
        Customize();
    }

    private static void LoadPrefs()
    {
        if (PlayerPrefs.HasKey("Sensitivity"))
        {
            Options.Sensitivity = PlayerPrefs.GetFloat("Sensitivity");
        }
        if (PlayerPrefs.HasKey("ShowingLevel"))
        {
            Options.ShowingLevel = PlayerPrefs.GetInt("ShowingLevel");
        }
        Options.IsLevelStart = false;
    }

    private void Customize()
    {
        Options.IsPlaying = true;

        if (Options.ShowingLevel < Level)
        {
            Options.ShowingLevel = Level;
        }

        if (Options.ShowingLevel < _levels.Count)
        {
            Options.Level = Options.ShowingLevel - 1;
        }
        else
        {
            Options.Level = Random.Range(3, _levels.Count);
        }

        _levelNumber.text = "Level " + Options.ShowingLevel;

        LevelMode level = _levels[Options.Level];

        if (Options.ShowingLevel < _levels.Count)
        {
            Score.NewInstance(level.CarsToWin);
        }
        else
        {
            Score.NewInstance(Random.Range(8, 13));
        }

        for (int i = 0; i < level.Roads.Count; i++)
        {
            int rotationY = level.Roads[i] ? 0 : 180;
            _roads[i].transform.rotation = Quaternion.Euler(0, rotationY, 0);
            _spawners[i].SetActive(true);
            _signs[i].SetActive(false);
        }

        for (int i = level.Roads.Count; i < _roads.Count; i++)
        {
            _signs[i].transform.localRotation = _roads[i].transform.rotation;
        }
    }
}
