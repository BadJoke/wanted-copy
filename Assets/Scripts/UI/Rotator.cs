﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    private void FixedUpdate()
    {
        transform.Rotate(Vector3.up);
    }
}