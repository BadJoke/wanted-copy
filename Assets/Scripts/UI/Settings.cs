﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] private Text _sensitivityValue;
    [SerializeField] private Slider _sensitivitySlider;
    [SerializeField] private GameObject _settings;
    [SerializeField] private Button _settingsButton;

    private void Start()
    {
        _sensitivityValue.text = string.Format("{0:0.00}", Options.Sensitivity / 4);
        _sensitivitySlider.value = Options.Sensitivity;
    }

    public void OnOpenClick()
    {
        Time.timeScale = 0;
        _settings.SetActive(true);
        _settingsButton.enabled = false;
    }

    public void OnCloseClick()
    {
        PlayerPrefs.SetFloat("Sensitivity", Options.Sensitivity);
        PlayerPrefs.Save();
        Time.timeScale = 1;

        _settings.SetActive(false);
        _settingsButton.enabled = true;
    }

    public void OnSensitivityChanged(float sensitivity)
    {
        Options.Sensitivity = sensitivity;
        _sensitivityValue.text = string.Format("{0:0.00}", Options.Sensitivity / 4);
    }
}
