﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinPanelController : MonoBehaviour
{
    [SerializeField]
    private Text _message;
    [SerializeField]
    private Cars _cars;
    [SerializeField]
    private GameObject _newCarPanel;
    [SerializeField]
    private Transform _newCar;

    private void Start()
    {
        _message.text = "LEVEL " + Options.ShowingLevel + " COMPLETED";

        int level = Options.ShowingLevel;
        if (level == 2 || level == 6 || level == 11 || level == 16)
        {
            int start = 0;
            int end = 0;

            switch (Options.ShowingLevel)
            {
                case 2:
                    start = 28;
                    end = 35;
                    break;
                case 6:
                    start = 35;
                    end = 42;
                    break;
                case 11:
                    start = 42;
                    end = 49;
                    break;
                case 16:
                    start = 49;
                    end = 56;
                    break;
            }

            _newCarPanel.SetActive(true);

            GameObject car = Instantiate(_cars.Prefabs[Random.Range(start, end)], _newCar);
            car.transform.localPosition = Vector3.zero;
            car.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
            car.transform.localScale = new Vector3(35, 35, 35);
            car.GetComponent<Rigidbody>().useGravity = false;
            car.GetComponent<BoxCollider>().enabled = false;
            car.GetComponent<CarMovement>().enabled = false;
            car.SetActive(true);
        }
    }

    public void OnNextClick()
    {
        StopAllCoroutines();
        Time.timeScale = 1;
        Options.ShowingLevel++;
        PlayerPrefs.SetInt("ShowingLevel", Options.ShowingLevel);
        PlayerPrefs.Save();
        SceneManager.LoadScene("Gameplay");
    }
}
