﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LosePannelController : MonoBehaviour
{
    [SerializeField] private Text _progress;
    [SerializeField] private CarCatcher _catcher;

    private void Start()
    {
        _progress.text = string.Format("{0:0}% COMPLETED", Score.GetInstance().BestProgress * 100);
    }

    public void OnRestartClick()
    {
        SceneManager.LoadScene("Gameplay");
        Time.timeScale = 1;
    }
}
