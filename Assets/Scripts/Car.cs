﻿using Assets.Scripts;
using UnityEngine;

[CreateAssetMenu(fileName = "Car", menuName = "Car")]
public class Car : ScriptableObject
{
    [SerializeField] private CarType Type;
    [SerializeField] private CarColor Color;
}
